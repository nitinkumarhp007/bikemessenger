package com.bikemessenger_app.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.bikemessenger_app.MainActivity;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    String message = "", friend_id = "", created = "", message_id = "", post_id = "", message_type = "", notification_code = "", product_id = "", username = "", sender_id = "", chat_message, timeStamp;

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    NotificationManager notificationManager;
    Notification notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());

        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());

        try {
            obj = new JSONObject(remoteMessage.getData());
            message = obj.getString("message");
            notification_code = obj.getString("notification_code");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        }
        if (notification_code.equals("3")) {//Like & Comment
            try {
                JSONObject object = new JSONObject(obj.getString("body"));
                post_id = object.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendNotification(getApplicationContext(), message);
        } else {
            sendNotification(getApplicationContext(), message);
        }


    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
    }

    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;

        if (notification_code.equals("8")) { //chat
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("friend_id", friend_id);
            intent.putExtra("friend_name", username);
            intent.putExtra("is_from_push", true);
            intent.putExtra("notification_code", notification_code);
        }else {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("is_from_push", true);
            intent.putExtra("notification_code", notification_code);
        }
        /*intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder notificationBuilder = null;
        if (savePref.getStringLatest("vibration_switch").equals("1")) {
            notificationBuilder = new Notification.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setOngoing(false)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } else {
            notificationBuilder = new Notification.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setOngoing(false)
                    .setVibrate(new long[]{500, 500, 500, 500})
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }



    *//*    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.noti_logo).setLargeIcon(icon1);;
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1);
        }*//*

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);*/

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

    /*    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.noti_logo).setLargeIcon(icon1);;
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    // offer_status
    private void publishResultsMessage(String message, String message_type, String created, String username, String message_id) {

        Intent intent = new Intent(util.NOTIFICATION_MESSAGE);
        intent.putExtra("message_type", message_type);
        intent.putExtra("message", message);
        intent.putExtra("created", created);
        intent.putExtra("username", username);
        intent.putExtra("id", message_id);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

}
