package com.bikemessenger_app.parser;


public class AllAPIS {

    public static final String BASE_URL = "http://18.222.15.62/apis/v1/";


    public static final String USERLOGIN = BASE_URL + "driver/login";
    public static final String USER_SIGNUP = BASE_URL + "driver/signup";
    public static final String VERIFY_OTP = BASE_URL + "driver/verify";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot-password";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "change-password";
    public static final String EDIT_PROFILE = BASE_URL + "driver/update";
    public static final String APP_INFO = BASE_URL + "app-information";

    public static final String NOTIFICATION = BASE_URL + "notification";
    public static final String DELIVERIES = BASE_URL + "deliveries";
    public static final String DELIVERY_UPDATE = BASE_URL + "delivery-update";
}
