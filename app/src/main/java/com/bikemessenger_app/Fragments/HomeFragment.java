package com.bikemessenger_app.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bikemessenger_app.Activities.SignInActivity;
import com.bikemessenger_app.Adapters.RequetsAdapter;
import com.bikemessenger_app.MainActivity;
import com.bikemessenger_app.ModelClasses.RequestModel;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsyncGet;
import com.bikemessenger_app.parser.GetAsyncPatch;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class HomeFragment extends Fragment {


    Context context;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;
    RequetsAdapter adapter = null;

    ArrayList<RequestModel> list;

    // ArrayList<RequestModel> list_main;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            NOTIFICATION_API();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void NOTIFICATION_API() {
        //ProgressDialog mDialog = util.initializeProgress(context);
       /* if (mDialog != null)
            mDialog.show();*/
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.NOTIFICATION + "?page=1&limit=2000", formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
               /* if (mDialog != null)
                    mDialog.dismiss();*/

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray data = jsonmainObject.getJSONObject("data").getJSONArray("result");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setDescriptions(object.getString("descriptions"));
                                requestModel.setFrom_address(object.getString("from_address"));
                                requestModel.setFrom_latitude(object.getString("from_latitude"));
                                requestModel.setFrom_longitude(object.getString("from_longitude"));
                                requestModel.setId(object.getString("id"));
                                requestModel.setName(object.getString("contactName"));
                                requestModel.setPayment_type(object.getString("payment_type"));
                                requestModel.setPrice(object.optString("price"));
                                requestModel.setStatus(object.optString("status"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setTo_address(object.optString("to_address"));
                                requestModel.setTo_latitude(object.optString("to_latitude"));
                                requestModel.setTo_longitude(object.optString("to_longitude"));
                                requestModel.setTotal_distance(object.optString("total_distance"));
                                requestModel.setDelivery_time(object.optString("delivery_min"));

                                requestModel.setClient_phone(object.optString("contactPhone"));

                                list.add(requestModel);
                            }

                            if (list.size() > 0) {

                                Collections.reverse(list);

                                adapter = new RequetsAdapter(context, list,HomeFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);


                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else
                                util.IOSDialog(context, jsonmainObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DELIVERY_UPDATE_API(String status, String id, int pos) {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.STATUS, status);//1-> taken 2-> complete 3-> I think I will taken
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPatch mAsync = new GetAsyncPatch(context, AllAPIS.DELIVERY_UPDATE + "/" + id, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {


                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.delivery_updated_successfully)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    if (status.equals("1")) {
                                        list.remove(pos);
                                    } else if (status.equals("3")) {
                                        list.get(pos).setStatus(status);
                                    }

                                    adapter.notifyDataSetChanged();
                                }
                            }).show();


                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}