package com.bikemessenger_app.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bikemessenger_app.Activities.SignInActivity;
import com.bikemessenger_app.Adapters.OrdersAdapter;
import com.bikemessenger_app.Adapters.RequetsAdapter;
import com.bikemessenger_app.ModelClasses.RequestModel;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OrdersFragment extends Fragment {
    Context context;
    @BindView(R.id.upcoming)
    Button upcoming;
    @BindView(R.id.past)
    Button past;
    @BindView(R.id.upcoming_view)
    View upcomingView;
    @BindView(R.id.past_view)
    View pastView;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    private SavePref savePref;
    Unbinder unbinder;

    ArrayList<RequestModel> list;

    //ArrayList<UserOrdersModel> list_main;

    public OrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        upcomingView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        pastView.setBackgroundColor(getResources().getColor(R.color.white));
        upcoming.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        past.setTextColor(getResources().getColor(R.color.black));

        if (ConnectivityReceiver.isConnected())
            DELIVERIES_API("1");
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.upcoming, R.id.past})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upcoming:
                upcomingView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                pastView.setBackgroundColor(getResources().getColor(R.color.white));
                upcoming.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                past.setTextColor(getResources().getColor(R.color.black));

                if (ConnectivityReceiver.isConnected())
                    DELIVERIES_API("1");
                else
                    util.IOSDialog(context, util.internet_Connection_Error);

                break;
            case R.id.past:
                pastView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                upcomingView.setBackgroundColor(getResources().getColor(R.color.white));
                past.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                upcoming.setTextColor(getResources().getColor(R.color.black));

                if (ConnectivityReceiver.isConnected())
                    DELIVERIES_API("2");
                else
                    util.IOSDialog(context, util.internet_Connection_Error);

                break;
        }
    }

    private void DELIVERIES_API(String status) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");//1-> in progress 2-> complete
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.DELIVERIES + "?page=1&limit=2000&status=" + status, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();

                if (mDialog != null)
                    mDialog.dismiss();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray data = jsonmainObject.getJSONObject("data").getJSONArray("result");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                RequestModel requestModel = new RequestModel();
                                requestModel.setDescriptions(object.getString("descriptions"));
                                requestModel.setFrom_address(object.getString("from_address"));
                                requestModel.setFrom_latitude(object.getString("from_latitude"));
                                requestModel.setFrom_longitude(object.getString("from_longitude"));
                                requestModel.setId(object.getString("id"));
                                requestModel.setName(object.getString("name"));
                                requestModel.setPayment_type(object.getString("payment_type"));
                                requestModel.setPrice(object.optString("price"));
                                requestModel.setStatus(object.optString("status"));
                                requestModel.setCreated(object.optString("created"));
                                requestModel.setTo_address(object.optString("to_address"));
                                requestModel.setTo_latitude(object.optString("to_latitude"));
                                requestModel.setTo_longitude(object.optString("to_longitude"));
                                requestModel.setPack_wieght(object.optString("pack_wieght"));
                                requestModel.setPack_description(object.optString("pack_description"));
                                requestModel.setTotal_distance(object.optString("total_distance"));
                                requestModel.setDelivery_time(object.optString("delivery_min"));

                                requestModel.setContactName(object.optString("contactName"));
                                requestModel.setClient_phone(object.optString("contactPhone"));

                                list.add(requestModel);
                            }

                            if (list.size() > 0) {

                                Collections.reverse(list);

                                OrdersAdapter adapter = new OrdersAdapter(context, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);


                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                errorMessage.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else
                                util.IOSDialog(context, jsonmainObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}