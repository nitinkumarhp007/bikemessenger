package com.bikemessenger_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bikemessenger_app.Activities.SignInActivity;
import com.bikemessenger_app.Fragments.HomeFragment;
import com.bikemessenger_app.Fragments.OrdersFragment;
import com.bikemessenger_app.Fragments.ProfileFragment;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity {

    MainActivity context;
    private SavePref savePref;
    BottomNavigationView navigation;
    @BindView(R.id.title)
    TextView title;

    String notification_code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ButterKnife.bind(this);

        savePref = new SavePref(context);

        Log.e("auth_key", savePref.getID());
        Log.e("auth_key", SavePref.getDeviceToken(this, "token"));
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        if (getIntent().getBooleanExtra("is_from_push", false)) {
            notification_code = getIntent().getStringExtra("notification_code");

            if (notification_code.equals("5")) {
                navigation.setSelectedItemId(R.id.navigation_order);
            } else {
                navigation.setSelectedItemId(R.id.navigation_home);
            }
        } else
            navigation.setSelectedItemId(R.id.navigation_home);


        Log.e("token____", SavePref.getDeviceToken(this, "token"));

        /*if (Environment.isExternalStorageManager()) {
            //todo when permission is granted
        } else {
            //request for the permission
            Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        }
*/

    }


    private boolean checkPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int result = ContextCompat.checkSelfPermission(MainActivity.this, READ_EXTERNAL_STORAGE);
            int result1 = ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    title.setText(getString(R.string.home));
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_order:
                    loadFragment(new OrdersFragment());
                    title.setText(getString(R.string.orders));
                    return true;
                case R.id.navigation_profile:
                    loadFragment(new ProfileFragment());
                    title.setText(getString(R.string.profile));
                    return true;
            }
            return false;
        }
    };

    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, SignInActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigation.setSelectedItemId(R.id.navigation_home);
                    }
                }).show();
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }


}