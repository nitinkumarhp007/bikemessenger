package com.bikemessenger_app.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class RequestModel implements Parcelable {

    String id="";
    String from_address="";
    String to_address="";
    String descriptions="";
    String to_latitude="";
    String to_longitude="";
    String from_latitude="";
    String from_longitude="";
    String total_distance="";
    String price="";
    String payment_type="";
    String name="";
    String status="";
    String created="";
    String pack_wieght="";
    String pack_description="";
    String delivery_time="";
    String contactName="";
    String client_phone="";

    public RequestModel()
    {}

    protected RequestModel(Parcel in) {
        id = in.readString();
        from_address = in.readString();
        to_address = in.readString();
        descriptions = in.readString();
        to_latitude = in.readString();
        to_longitude = in.readString();
        from_latitude = in.readString();
        from_longitude = in.readString();
        total_distance = in.readString();
        price = in.readString();
        payment_type = in.readString();
        name = in.readString();
        status = in.readString();
        created = in.readString();
        pack_wieght = in.readString();
        pack_description = in.readString();
        delivery_time = in.readString();
        contactName = in.readString();
        client_phone = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(from_address);
        dest.writeString(to_address);
        dest.writeString(descriptions);
        dest.writeString(to_latitude);
        dest.writeString(to_longitude);
        dest.writeString(from_latitude);
        dest.writeString(from_longitude);
        dest.writeString(total_distance);
        dest.writeString(price);
        dest.writeString(payment_type);
        dest.writeString(name);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(pack_wieght);
        dest.writeString(pack_description);
        dest.writeString(delivery_time);
        dest.writeString(contactName);
        dest.writeString(client_phone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestModel> CREATOR = new Creator<RequestModel>() {
        @Override
        public RequestModel createFromParcel(Parcel in) {
            return new RequestModel(in);
        }

        @Override
        public RequestModel[] newArray(int size) {
            return new RequestModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_address() {
        return from_address;
    }

    public void setFrom_address(String from_address) {
        this.from_address = from_address;
    }

    public String getTo_address() {
        return to_address;
    }

    public void setTo_address(String to_address) {
        this.to_address = to_address;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getTo_latitude() {
        return to_latitude;
    }

    public void setTo_latitude(String to_latitude) {
        this.to_latitude = to_latitude;
    }

    public String getTo_longitude() {
        return to_longitude;
    }

    public void setTo_longitude(String to_longitude) {
        this.to_longitude = to_longitude;
    }

    public String getFrom_latitude() {
        return from_latitude;
    }

    public void setFrom_latitude(String from_latitude) {
        this.from_latitude = from_latitude;
    }

    public String getFrom_longitude() {
        return from_longitude;
    }

    public void setFrom_longitude(String from_longitude) {
        this.from_longitude = from_longitude;
    }

    public String getTotal_distance() {
        return total_distance;
    }

    public void setTotal_distance(String total_distance) {
        this.total_distance = total_distance;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getPack_wieght() {
        return pack_wieght;
    }

    public void setPack_wieght(String pack_wieght) {
        this.pack_wieght = pack_wieght;
    }

    public String getPack_description() {
        return pack_description;
    }

    public void setPack_description(String pack_description) {
        this.pack_description = pack_description;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getClient_phone() {
        return client_phone;
    }

    public void setClient_phone(String client_phone) {
        this.client_phone = client_phone;
    }
}
