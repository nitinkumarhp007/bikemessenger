package com.bikemessenger_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bikemessenger_app.Activities.DetailActivity;
import com.bikemessenger_app.Activities.OrderDetailActivity;
import com.bikemessenger_app.ModelClasses.RequestModel;
import com.bikemessenger_app.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<RequestModel> list;

    public OrdersAdapter(Context context, ArrayList<RequestModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.orders_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getId()).error(R.drawable.logo).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.pickup_address.setText("R: " + list.get(position).getFrom_address());
        holder.delivery_address.setText("C:" + list.get(position).getTo_address());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.delivery_address)
        TextView delivery_address;
        @BindView(R.id.delivery_method)
        TextView delivery_method;
        @BindView(R.id.pickup_address)
        TextView pickup_address;
        @BindView(R.id.detail)
        TextView detail;
        @BindView(R.id.time)
        TextView time;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
