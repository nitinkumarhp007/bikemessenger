package com.bikemessenger_app.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bikemessenger_app.Activities.DetailActivity;
import com.bikemessenger_app.Fragments.HomeFragment;
import com.bikemessenger_app.ModelClasses.RequestModel;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.util;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class RequetsAdapter extends RecyclerView.Adapter<RequetsAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    HomeFragment homeFragment;
    ArrayList<RequestModel> tempList;
    ArrayList<RequestModel> list;
    private View view;

    public RequetsAdapter(Context context, ArrayList<RequestModel> list, HomeFragment homeFragment) {
        this.context = context;
        this.homeFragment = homeFragment;
        this.list = list;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.requests_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getId()).error(R.drawable.logo).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.pickup_address.setText("R: " + list.get(position).getFrom_address());
        holder.delivery_address.setText("C:" + list.get(position).getTo_address());

        if (list.get(position).getStatus().equals("3")) {
            holder.i_think_i_will_take_it.setBackground(context.getResources().getDrawable(R.drawable.drawable_button_gray));
        } else {
            holder.i_think_i_will_take_it.setBackground(context.getResources().getDrawable(R.drawable.drawable_button));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);
            }
        });

        holder.i_think_i_will_take_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list.get(position).getStatus().equals("0"))
                {
                    homeFragment.DELIVERY_UPDATE_API("3",list.get(position).getId(),position);
                }
                else {
                    util.IOSDialog(context, context.getString(R.string.already_performedthis_action));
                }

            }
        });
        holder.taken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.DELIVERY_UPDATE_API("1",list.get(position).getId(),position);
            }
        });

        holder.time.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getCreated())));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.delivery_address)
        TextView delivery_address;
        @BindView(R.id.delivery_method)
        TextView delivery_method;
        @BindView(R.id.pickup_address)
        TextView pickup_address;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.i_think_i_will_take_it)
        TextView i_think_i_will_take_it;
        @BindView(R.id.taken)
        TextView taken;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<RequestModel> nList = new ArrayList<RequestModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (RequestModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }

}
