package com.bikemessenger_app.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bikemessenger_app.MainActivity;
import com.bikemessenger_app.ModelClasses.RequestModel;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsync;
import com.bikemessenger_app.parser.GetAsyncPatch;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DetailActivity extends AppCompatActivity {
    DetailActivity context;
    private SavePref savePref;

    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.delivery_address)
    TextView delivery_address;
    @BindView(R.id.delivery_method)
    TextView delivery_method;
    @BindView(R.id.pickup_address)
    TextView pickup_address;
    @BindView(R.id.client_phone)
    TextView client_phone;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.total_amount)
    TextView total_amount;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.notes)
    TextView notes;
    @BindView(R.id.i_think_i_will_take_it)
    Button i_think_i_will_take_it;
    @BindView(R.id.taken)
    Button taken;
    @BindView(R.id.closed)
    Button closed;
    RequestModel requestModel;
    CountDownTimer yourCountDownTime;
    String status_text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        context = DetailActivity.this;
        savePref = new SavePref(context);

        requestModel = getIntent().getExtras().getParcelable("data");


        Long delivery_time = Long.valueOf(requestModel.getDelivery_time());

        Log.e("seconds__", String.valueOf(delivery_time));

        Long tsLong = System.currentTimeMillis() / 1000;

        Log.e("seconds__", "current " + String.valueOf(tsLong));

        Long now = ((delivery_time - tsLong) * 1000);
        // Long now = (delivery_time * 1000);

        Log.e("seconds__", "Now" + String.valueOf(now));

        yourCountDownTime = new CountDownTimer(now, 1000) {

            public void onTick(long millisUntilFinished) {




              /*  int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                price.setText("Delivery Deadline: " + String.format("%02d", minutes) + " min "
                        + "," + String.format("%02d", seconds) + " sec");*/

                // Log.e("seconds__", "before" + String.valueOf(millisUntilFinished));

                int seconds = (int) (millisUntilFinished / 1000) % 60;

                //Log.e("seconds__", "after" + String.valueOf(seconds));

                int day = (int) TimeUnit.SECONDS.toDays(seconds);
                int minute = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
                long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);


                if (day == 0) {
                    price.setText(getString(R.string.delivery_deadline) + ": " + hours + " ore, " + minute + " min, " + second + " sec");
                } else {
                    price.setText(getString(R.string.delivery_deadline) + ": " + day + " day, " + hours + " ore, " + minute + " min, " + second + " sec");
                }

            }

            public void onFinish() {
                price.setText(getString(R.string.delivery_deadline_finished));
            }

        }.start();


        taken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (status_text.equals("3")) {
                    if (ConnectivityReceiver.isConnected())
                        DELIVERY_UPDATE_API("1");
                    else
                        util.IOSDialog(context, util.internet_Connection_Error);
                } else {
                    util.IOSDialog(context, getString(R.string.please_first_press_i_think_i_will_take));
                }*/

                if (ConnectivityReceiver.isConnected())
                    DELIVERY_UPDATE_API("1");
                else
                    util.IOSDialog(context, util.internet_Connection_Error);

            }
        });

        i_think_i_will_take_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (status_text.equals("0")) {
                    if (ConnectivityReceiver.isConnected())
                        DELIVERY_UPDATE_API("3");
                    else
                        util.IOSDialog(context, util.internet_Connection_Error);
                } else {
                    util.IOSDialog(context, getString(R.string.already_performedthis_action));
                }

            }
        });

        status_text = requestModel.getStatus();

        Log.e("status_text", status_text);

        if (status_text.equals("0")) {
            status.setText(getString(R.string.requested));
            i_think_i_will_take_it.setBackground(getResources().getDrawable(R.drawable.drawable_button));
        } else if (status_text.equals("3")) {
            status.setText(getString(R.string.accepted));
            i_think_i_will_take_it.setBackground(getResources().getDrawable(R.drawable.drawable_button_gray));
        }

        setToolbar();

        setdata();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (yourCountDownTime != null)
            yourCountDownTime.cancel();
    }

    private void DELIVERY_UPDATE_API(String status) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.STATUS, status);//1-> taken 2-> complete 3-> I think I will taken
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPatch mAsync = new GetAsyncPatch(context, AllAPIS.DELIVERY_UPDATE + "/" + requestModel.getId(), formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            status_text = status;

                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.delivery_updated_successfully)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    if (status.equals("1")) {

                                        Intent intent = new Intent(context, MainActivity.class);
                                        intent.putExtra("from_booking", true);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                                    } else if (status.equals("3")) {
                                        i_think_i_will_take_it.setBackground(getResources().getDrawable(R.drawable.drawable_button_gray));
                                    }


                                }
                            }).show();


                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setdata() {
        Glide.with(context).load(requestModel.getId()).error(R.drawable.logo).into(image);
        name.setText(requestModel.getName());
        pickup_address.setText("R: " + requestModel.getFrom_address());
        delivery_address.setText("C:" + requestModel.getTo_address());
        client_phone.setText("Telefono cliente: " + requestModel.getClient_phone());
        notes.setText(getString(R.string.notes) + ": " + Html.fromHtml(requestModel.getDescriptions()));

        if (requestModel.getPayment_type().equals("1"))
            delivery_method.setText(getString(R.string.delivery_method_cash));
        else
            delivery_method.setText(getString(R.string.delivery_method_online));

        price.setText(getString(R.string.delivery_method_online) + " : 5:45 PM");
        total_amount.setText(getString(R.string.total_amount) + ": €" + requestModel.getPrice());


    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}