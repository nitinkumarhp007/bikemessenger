package com.bikemessenger_app.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsync;
import com.bikemessenger_app.parser.GetAsyncPUT;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateInfoActivity extends AppCompatActivity {
    UpdateInfoActivity context;
    private SavePref savePref;
    @BindView(R.id.package_weight)
    EditText package_weight;
    @BindView(R.id.package_description)
    EditText package_description;
    @BindView(R.id.update)
    Button update;
    String order_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info);
        ButterKnife.bind(this);
        context = UpdateInfoActivity.this;
        savePref = new SavePref(context);
        setToolbar();

        order_id = getIntent().getStringExtra("order_id");


        package_weight.setText(getIntent().getStringExtra("pack_wieght"));
        package_description.setText(getIntent().getStringExtra("pack_description"));


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    if (package_weight.getText().toString().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_package_weight));
                        update.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                    } else if (package_description.getText().toString().isEmpty()) {
                        util.IOSDialog(context, getString(R.string.enter_package_description));
                        update.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                    } else {
                        DELIVERY_UPDATE_API();
                    }
                } else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });
    }

    private void DELIVERY_UPDATE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PACK_WEIGHT, package_weight.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PACK_DESCRIPTION, package_description.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPUT mAsync = new GetAsyncPUT(context, AllAPIS.DELIVERY_UPDATE + "/" + order_id, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, getString(R.string.order_updated_sucessfully));
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("pack_wieght", package_weight.getText().toString().trim());
                            returnIntent.putExtra("pack_description", package_description.getText().toString().trim());
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.update_info);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}