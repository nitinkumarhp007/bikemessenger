package com.bikemessenger_app.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bikemessenger_app.ModelClasses.RequestModel;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsyncPUT;
import com.bikemessenger_app.parser.GetAsyncPatch;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.media.MediaExtractor.MetricsConstants.FORMAT;

public class OrderDetailActivity extends AppCompatActivity {
    OrderDetailActivity context;
    private SavePref savePref;
    @BindView(R.id.update_info)
    Button update_info;
    @BindView(R.id.closed)
    Button closed;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.delivery_address)
    TextView delivery_address;
    @BindView(R.id.delivery_method)
    TextView delivery_method;
    @BindView(R.id.pickup_address)
    TextView pickup_address;
    @BindView(R.id.client_phone)
    TextView client_phone;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.total_amount)
    TextView total_amount;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.name_contact)
    TextView name_contact;
    @BindView(R.id.notes)
    TextView notes;
    CountDownTimer yourCountDownTime;
    RequestModel requestModel;

    String change_text = "", status_text = "", pack_wieght = "", pack_description = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);

        context = OrderDetailActivity.this;
        savePref = new SavePref(context);

        requestModel = getIntent().getExtras().getParcelable("data");

        pack_wieght = requestModel.getPack_wieght();
        pack_description = requestModel.getPack_description();


        update_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   Intent intent = new Intent(context, UpdateInfoActivity.class);
                intent.putExtra("order_id", requestModel.getId());
                intent.putExtra("pack_wieght", pack_wieght);
                intent.putExtra("pack_description", pack_description);
                startActivityForResult(intent, 200);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/

                if (requestModel.getPayment_type().equals("1")) {
                    //cash
                    showDialog(context);
                } else {
                    DELIVERY_UPDATE_API("0", "0", "0");
                }

               /* if (!pack_description.isEmpty()) {
                    if (requestModel.getPayment_type().equals("1")) {
                        //cash
                        showDialog(context);
                    } else {
                        DELIVERY_UPDATE_API("0", "0", "0");
                    }
                } else {
                    util.IOSDialog(context, getString(R.string.please_first_update_delivery_info));
                }*/

            }
        });

        closed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (!pack_description.isEmpty()) {
                    if (requestModel.getPayment_type().equals("1")) {
                        //cash
                        showDialog(context);
                    } else {
                        DELIVERY_UPDATE_API("0", "0", "0");
                    }
                } else {
                    util.IOSDialog(context, getString(R.string.please_first_update_delivery_info));
                }*/


                if (requestModel.getPayment_type().equals("1")) {
                    //cash
                    showDialog(context);
                } else {
                    DELIVERY_UPDATE_API("0", "0", "0");
                }


            }
        });

        status_text = requestModel.getStatus();


        if (status_text.equals("2")) {
            status.setText("Completata");
            closed.setVisibility(View.INVISIBLE);
            update_info.setVisibility(View.INVISIBLE);
            price.setText(getString(R.string.delivery_deadline_completed));

        } else {
            countdown();
            status.setText("Attiva");
        }


        setToolbar();
        setdata();
    }

    private void countdown() {
        Long delivery_time = Long.valueOf(requestModel.getDelivery_time());

        Log.e("seconds__", String.valueOf(delivery_time));

        Long tsLong = System.currentTimeMillis() / 1000;

        Log.e("seconds__", "current " + String.valueOf(tsLong));

        Long now = ((delivery_time - tsLong) * 1000);
        // Long now = (delivery_time * 1000);

        Log.e("seconds__", "Now" + String.valueOf(now));

        yourCountDownTime = new CountDownTimer(now, 1000) {

            public void onTick(long millisUntilFinished) {




              /*  int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                price.setText("Delivery Deadline: " + String.format("%02d", minutes) + " min "
                        + "," + String.format("%02d", seconds) + " sec");*/

                // Log.e("seconds__", "before" + String.valueOf(millisUntilFinished));

                int seconds = (int) (millisUntilFinished / 1000) % 60;

                //Log.e("seconds__", "after" + String.valueOf(seconds));

                int day = (int) TimeUnit.SECONDS.toDays(seconds);
                int minute = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);
                long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);


                if (day == 0) {
                    price.setText(getString(R.string.delivery_deadline) + ": " + hours + " ore, " + minute + " min, " + second + " sec");
                } else {
                    price.setText(getString(R.string.delivery_deadline) + ": " + day + " day, " + hours + " ore, " + minute + " min, " + second + " sec");
                }

            }

            public void onFinish() {
                price.setText(getString(R.string.delivery_deadline_finished));
            }

        }.start();

    }


    private void DELIVERY_UPDATE_API(String amount, String given_back, String change) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.STATUS, "2");//1-> taken 2-> complete 3-> I think I will taken
        formBuilder.addFormDataPart(Parameters.AMOUNT, amount);
        formBuilder.addFormDataPart(Parameters.GIVEN_BACK, given_back);
        formBuilder.addFormDataPart(Parameters.CHANGE, change);
        formBuilder.addFormDataPart(Parameters.PACK_DESCRIPTION, pack_description);
        formBuilder.addFormDataPart(Parameters.PACK_WEIGHT, pack_wieght);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPatch mAsync = new GetAsyncPatch(context, AllAPIS.DELIVERY_UPDATE + "/" + requestModel.getId(), formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.deliverycompletedsuccessfully)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    finish();
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 200) {
            pack_wieght = data.getStringExtra("pack_wieght");
            pack_description = data.getStringExtra("pack_description");
        }
    }

    private void setdata() {
        Glide.with(context).load(requestModel.getId()).error(R.drawable.logo).into(image);
        name.setText(requestModel.getName());
        name_contact.setText(requestModel.getContactName());
        pickup_address.setText("R: " + requestModel.getFrom_address());
        delivery_address.setText("C:" + requestModel.getTo_address());
        client_phone.setText("Telefono cliente: " + requestModel.getClient_phone());
        notes.setText(context.getResources().getString(R.string.notes) + ": " + Html.fromHtml(requestModel.getDescriptions()));

        if (requestModel.getPayment_type().equals("1"))
            delivery_method.setText(getString(R.string.delivery_method_cash));
        else
            delivery_method.setText(getString(R.string.delivery_method_online));

        //price.setText("Delivery Deadline : 5:45 PM");
        total_amount.setText(getString(R.string.total_amount) + ": €" + requestModel.getPrice());
    }


    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_new);

        EditText amount_taken = (EditText) dialog.findViewById(R.id.amount_taken);
        EditText given_back = (EditText) dialog.findViewById(R.id.given_back);
        TextView change = (TextView) dialog.findViewById(R.id.change);
        RadioButton return_back = (RadioButton) dialog.findViewById(R.id.return_back);
        RadioButton pending = (RadioButton) dialog.findViewById(R.id.pending);

        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        Button done = (Button) dialog.findViewById(R.id.done);
        dialog.show();

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, change);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu__, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals(R.string.given_back))
                            change_text = "1";
                        else if (item.getTitle().equals(R.string.pending))
                            change_text = "2";

                        change.setText(item.getTitle());
                        popup.dismiss();


                        return true;
                    }
                });
                popup.show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityReceiver.isConnected()) {
                    if (given_back.getText().toString().isEmpty()) {
                        if (amount_taken.getText().toString().isEmpty()) {
                            util.IOSDialog(context, getString(R.string.amout_token));
                            done.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                        } else {
                            dialog.dismiss();
                            DELIVERY_UPDATE_API(amount_taken.getText().toString().trim(), "0", "0");
                        }
                    } else {
                        if (amount_taken.getText().toString().isEmpty()) {
                            util.IOSDialog(context, getString(R.string.amout_token));
                            done.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                        } /*else if (change.getText().toString().isEmpty()) {
                            util.IOSDialog(context, getString(R.string.change_given_back));
                            done.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                        }*/ else {
                            dialog.dismiss();

                            if (return_back.isChecked())
                                change_text = "1";
                            else if (pending.isChecked())
                                change_text = "2";

                            DELIVERY_UPDATE_API(amount_taken.getText().toString().trim(),
                                    given_back.getText().toString().trim(), change_text);
                        }
                    }

                } else
                    util.IOSDialog(context, util.internet_Connection_Error);


            }
        });


    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.order_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (yourCountDownTime != null)
            yourCountDownTime.cancel();
    }


}