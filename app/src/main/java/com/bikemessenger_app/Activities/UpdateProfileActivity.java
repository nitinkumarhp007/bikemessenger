package com.bikemessenger_app.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends AppCompatActivity {
    UpdateProfileActivity context;
    private SavePref savePref;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email_address)
    TextView emailAddress;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.update)
    Button update;


    private String selectedimage = "";
    Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);
        setToolbar();

        context = UpdateProfileActivity.this;
        savePref = new SavePref(context);

        setdata();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(context);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EDIT_PROFILETask();
            }
        });

    }

    private void setdata() {
        name.setText(savePref.getName());
        emailAddress.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
        Glide.with(context).load(savePref.getImage()).error(R.drawable.logo).into(image);
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.update_profile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void EDIT_PROFILETask() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_your_name));
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_email));
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, getString(R.string.enter_vaild_email));
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_phone));
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                EDIT_PROFILE_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }


    private void EDIT_PROFILE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString());

        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setEmail(body.getString("email"));
                            savePref.setPhone(body.optString("phone"));
                            savePref.setName(body.getString("name"));
                            savePref.setImage(body.getString("profile"));

                            finish();
                            util.showToast(context, getString(R.string.profile_update_successfully));

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                Glide.with(this).load(selectedimage).into(image);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
}