package com.bikemessenger_app.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bikemessenger_app.MainActivity;
import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.GPSTracker;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsync;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity {
    SignUpActivity context;
    @BindView(R.id.country_code)
    TextView countryCode;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.privacy)
    CheckBox privacy;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.privacy_text)
    TextView privacyText;

    private String selectedimage = "";
    Uri fileUri;

    GPSTracker gpsTracker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        context = SignUpActivity.this;
        savePref = new SavePref(context);

        gpsTracker = new GPSTracker(context);

    }

    @OnClick({R.id.image, R.id.back_button, R.id.privacy_text, R.id.sign_up, R.id.country_code, R.id.sign_in})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.privacy_text:
                Intent intent11111 = new Intent(context, TermConditionActivity.class);
                intent11111.putExtra("type", "privacy");
                startActivity(intent11111);
                break;
            case R.id.back_button:
                finish();
                break;
            case R.id.sign_up:
                SignUpTask();
                break;
            case R.id.country_code:
                CountryPicker picker = CountryPicker.newInstance(getString(R.string.select_country));  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code1, String dialCode, int flagDrawableResID) {
                        picker.dismiss();
                        countryCode.setText(dialCode);
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SignUpTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (selectedimage.isEmpty()) {
                util.IOSDialog(context, getString(R.string.select_profile_image));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_your_name));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_email));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, getString(R.string.enter_vaild_email));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_phone));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, getString(R.string.please_enter_password));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!privacy.isChecked()) {
                util.IOSDialog(context, getString(R.string.check_privacy_policy));
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                USER_SIGNUP_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }


    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString());
        formBuilder.addFormDataPart(Parameters.PHONE, countryCode.getText().toString().trim().substring(1) + phone.getText().toString());
        formBuilder.addFormDataPart(Parameters.LOCATION, getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude()));
        formBuilder.addFormDataPart(Parameters.LATITUDE, String.valueOf(gpsTracker.getLatitude()));
        formBuilder.addFormDataPart(Parameters.LONGITUDE, String.valueOf(gpsTracker.getLongitude()));

        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");//1-> android 2-> ios non required
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(context, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.account_created)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(context, SignInActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            })
                                    /* .setNegativeButton("Cancel", null)*/.show();

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                Glide.with(this).load(selectedimage).into(image);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                // Log.e("My Current loction address", strReturnedAddress.toString());
            } else {
                // Log.e("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }
}