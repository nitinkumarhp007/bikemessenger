package com.bikemessenger_app.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bikemessenger_app.R;
import com.bikemessenger_app.UtilFiles.ConnectivityReceiver;
import com.bikemessenger_app.UtilFiles.Parameters;
import com.bikemessenger_app.UtilFiles.SavePref;
import com.bikemessenger_app.UtilFiles.util;
import com.bikemessenger_app.parser.AllAPIS;
import com.bikemessenger_app.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChangePasswordActivity extends AppCompatActivity {
    ChangePasswordActivity context;
    private SavePref savePref;
    @BindView(R.id.current_password)
    EditText currentPassword;
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.confirm_new_password)
    EditText confirmNewPassword;
    @BindView(R.id.change)
    Button change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);
        context = ChangePasswordActivity.this;
        savePref = new SavePref(context);
        setToolbar();

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangePasswordTask();
            }
        });
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.change_password));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void ChangePasswordTask() {
        if (ConnectivityReceiver.isConnected()) {
            if (currentPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_current_password));
                change.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            } else if (newPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_new_password));
                change.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            } else if (confirmNewPassword.getText().toString().isEmpty()) {
                util.IOSDialog(context, getString(R.string.enter_confirm_password));
                change.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            } else if (!newPassword.getText().toString().equals(confirmNewPassword.getText().toString())) {
                util.IOSDialog(context, getString(R.string.password_not_match));
                change.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
            } else {
                CHANGE_PASSWORD_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void CHANGE_PASSWORD_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OLD_PASSWORD, currentPassword.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.NEW_PASSWORD, newPassword.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHANGEPASSWORD, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, getString(R.string.password_updated));
                            finish();
                            util.hideKeyboard(context);
                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}